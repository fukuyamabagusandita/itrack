<html>
   <head>
   	  <title> APPS PROFILE SETTING </title>
   </head>
   <body bgcolor="darkblue">
   	  <style type="text/css">
         .center_form li {
         	background-color: white;
         	box-shadow: 1px 1px 1px 1px grey;
         	border-radius: 10px;
         	position: absolute;
         	color: grey;
         	font-family: sans-serif;
         	width: 40%;
         	height: 50%;
         	margin: 10% 28%;
         }

         .center_form ul > li {
         	display: block;
         }

         .address {
            position: absolute;
            box-shadow: 1px 1px 1px 1px grey;
            font-family: sans-serif;
            padding: 2%;
            border: none;
            border-radius: 20px;
            color: grey;
            width: 250px;
            height: 30px;
            margin: 10% 27%;
         }

         .port {
            position: absolute;
            box-shadow: 1px 1px 1px 1px grey;
            font-family: sans-serif;
            padding: 2%;
            border: none;
            border-radius: 20px;
            color: grey;
            width: 250px;
            height: 30px;
            margin: 15% 27%;
         }

         .password {
            position: absolute;
            box-shadow: 1px 1px 1px 1px grey;
            font-family: sans-serif;
            padding: 2%;
            border: none;
            border-radius: 20px;
            color: grey;
            width: 250px;
            height: 30px;
            margin: 24% 27%;
         }

         .socket {
            position: absolute;
            box-shadow: 1px 1px 1px 1px grey;
            font-family: sans-serif;
            padding: 2%;
            border: none;
            border-radius: 20px;
            color: grey;
            margin: 28% 27%;
         }

         .button_save {
         	background-color: darkorange;
         	width: 100px;
         	border: none;
         	border-radius: 10px;
         	height: 35px;
         	position: absolute;
         	color: white;
         	font-family: sans-serif;
         	margin: 30% 27%;
         }

         .button_delete {
         	background-color: darkorange;
         	width: 100px;
         	border: none;
         	border-radius: 10px;
         	height: 35px;
         	position: absolute;
         	color: white;
         	font-family: sans-serif;
         	margin: 30% 55%;
         }

         .back li {
         	background-color: darkorange;
         	font-family: sans-serif;
         	color: white;
         	text-align:center;
         	position: absolute;
         	width: 100px;
         	height: 50px;
         	margin: 10px 10px;
         }

         .back ul > li {
         	display: block;
         }

         .result_form li {
         	background-color: darkgreen;
         	box-shadow: 1px 1px 1px 1px grey;
         	border-radius: 10px;
         	padding: 2% 2%;
         	position: absolute;
         	color: white;
         	font-family: sans-serif;
         	width: 30%;
         	height: 30%;
         	margin: 15% 60%;
         }

         .result_form ul > li {
         	display: block;
         }

   	  </style> 
   	  <div class="center_form">
   	  	 <ul>
   	  	 	<li>
                <form method="POST" action="profile.php">
                    <input name="ipaddress" type="text" placeholder=" Your ip address ... " class="address">
                    <br>
                    <input name="myport" type="text" placeholder=" Your port ... " class="port">
                    <input name="password" type="password" placeholder=" Your password ... " class="password">
                    <br>
                    <button type="submit" class="button_save"> SAVE </button>
                    <button type="reset" class="button_delete"> DELETE </button>
                </form>
   	  	 	</li>
   	  	 </ul>
   	  </div>
   	  <?php
   	        $ip = $_POST['ipaddress'];
   	        $port = $_POST['myport'];
   	        $password = $_POST['password'];

            $hash = password_hash($password, PASSWORD_DEFAULT);
   	        $address_bar = getprotobyname($ip);
   	        $port_bar = getprotobynumber($port);
   	        $pass = password_verify($password, $hash);
   	  ?>
   	  <a href="index.php"><div class="back">
   	  	<ul>
   	  		<li>
   	  			<p><strong> HOME </strong></p>
   	  		</li>
   	  	</ul>
   	  </div></a>
   	  <div class="result_form">
   	  	<ul>
   	  		<li>
                  <strong> RESULT </strong>
                  <p> YOUR IP ADDRESS : <?php echo $ip; ?></p>
                  <p> YOUR PORT  : <?php echo $port; ?></p>
                  <p> YOUR PASSWORD : <?php echo $password; ?></p>
                  <p> Veryfied password : <?php echo $pass; ?></p>
                  <p> Port bar value : <?php echo $port_bar; ?></p>
   	  		</li>
   	  	</ul>
   	  </div>
   </body>
</html>