<html>
  <head>
  	 <title> HOME </title>
  </head>
  <body>
    <style type="text/css">

    	.form_up li {
    		background-color: darkblue;
    		left:0;
    		text-decoration: none;
    		width: 100%;
    		height: 20%;
    		position: absolute;
    		margin: -1% 0;
    	}

    	.form_up ul > li {
    		display: block;
    	}
        
        .form_center li {
        	background-color: white;
        	box-shadow: 1px 1px 1px 1px grey;
        	color: black;
        	font-family: sans-serif;
            position: absolute;
            width: 35%;
            height: 45%;
            margin: 15% 30%;
        }

        .form_center ul > li {
        	display: block;
        }


        .url {
        	background-color: white;
        	box-shadow: 1px 1px 1px 1px 1px grey;
        	position: absolute;
        	color: grey;
        	border-radius: 20px;
        	font-family: sans-serif;
        	width: 200px;
        	height: 25px;
        	margin: 50px 130px;
        }

        .username {
        	background-color: white;
        	box-shadow: 1px 1px 1px 1px 1px grey;
        	position: absolute;
        	color: grey;
        	border-radius: 20px;
        	font-family: sans-serif;
        	width: 200px;
        	height: 25px;
        	margin: 90px 130px;
        }

        .password {
        	background-color: white;
        	box-shadow: 1px 1px 1px 1px 1px grey;
        	position: absolute;
        	color: grey;
        	border-radius: 20px;
        	font-family: sans-serif;
        	width: 200px;
        	height: 25px;
        	margin: 130px 130px;
        }

        .button_send:hover {
        	background-color: red;
        	font-family: sans-serif;
        	position: absolute;
        	color: white;
        	width: 90px;
        	height: 40px;
        	border-radius: 15px;
        	border: none;
        	box-shadow: 1px 1px 1px 1px grey;
        	margin: 150px 130px;
        }

        .button_send {
        	background-color: darkorange;
        	font-family: sans-serif;
        	position: absolute;
        	color: white;
        	width: 90px;
        	height: 40px;
        	border-radius: 15px;
        	border: none;
        	box-shadow: 1px 1px 1px 1px grey;
        	margin: 150px 130px;
        }

        .button_delete:hover {
        	background-color: red;
        	font-family: sans-serif;
        	position: absolute;
        	color: white;
        	width: 90px;
        	height: 40px;
        	border-radius: 15px;
        	border: none;
        	box-shadow: 1px 1px 1px 1px grey;
        	margin: 150px 235px;
        }
        
        .button_delete {
        	background-color: darkorange;
        	font-family: sans-serif;
        	position: absolute;
        	color: white;
        	width: 90px;
        	height: 40px;
        	border-radius: 15px;
        	border: none;
        	box-shadow: 1px 1px 1px 1px grey;
        	margin: 150px 235px;
        }

    </style>
    <div class="form_up">
    	<ul>
    		<li>
    	    </li>
    	</ul>
    </div>
    <div class="form_center">
    	<ul>
    		<li>
                <form method="POST" action="result.php">
                	<input name="address" type="text" placeholder=" IP Address target ... " class="url">

                	<input name="tcp" type="text" placeholder=" PROTOCOL ... " class="username">

                	<input name="port" type="text" placeholder=" PORT ... " class="password">
                	<br>
                	<br>
                	<button type="submit" class="button_send"><strong> SEND </strong></button>
                	<button type="reset" class="button_delete"><strong> DELETE </strong></button>
                </form>
    		</li>
        </ul>
    </div>
  </body>
</html>